# @guilty/eslint-config

Standard eslint config for [Guilty AS](https://guilty.no) projects

# Use these rules in your project

## Installation
Add @guilty/eslint-config as a dependency: 

### yarn:

`$ yarn add eslint @guilty/eslint-config --dev`

### npm:  

`$ npm i eslint @guilty/eslint-config --dev`


## Configuration
### `package.json`
````
"eslintConfig": {
  "extends": "@guilty/eslint-config"
},
````

### `.eslintrc`
```
{
  extends: ["@guilty/eslint-config"]
}
````